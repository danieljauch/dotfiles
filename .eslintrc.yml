root: true
parser: babel-eslint
extends:
  - standard
  - standard-react
  - plugin:jest/recommended
  - plugin:import/recommended
  - plugin:prettier/recommended
  - prettier/react
  - prettier/standard
  - plugin:import/react-native
  - plugin:jsx-a11y/recommended
plugins:
  - flowtype
  - import
  - extra-rules
  - react
  - react-native
settings:
  import/resolver:
    node:
      extensions:
        - .jsx
        - .js
globals:
  fetch: true
rules:
  # Standard ESLint rules
  array-callback-return: "error"
  block-scoped-var: "error"
  brace-style: ["error", "1tbs", { allowSingleLine: true }]
  camelcase: 0
  consistent-this: "error"
  default-case: "error"
  dot-location: "error"
  dot-notation: "error"
  guard-for-in: "error"
  indent: ["error", 2]
  jsx-quotes: ["error", "prefer-double"]
  max-classes-per-file: ["error", 1]
  max-depth: "error"
  max-len: ["error", { code: 100, comments: 80, tabWidth: 2, ignoreUrls: true, ignoreStrings: true, ignoreTemplateLiterals: true, ignoreRegExpLiterals: true }]
  max-lines: ["warn", { max: 300, skipBlankLines: true, skipComments: true }]
  max-lines-per-function: ["warn", { max: 30, skipBlankLines: true, skipComments: true }]
  max-nested-callbacks: ["warn", 3]
  max-params: ["warn", 3]
  new-cap: "error"
  no-console: "error"
  no-else-return: "error"
  no-empty-function: "error"
  no-eq-null: "error"
  no-extend-native: "error"
  no-implicit-coercion: "error"
  no-multiple-empty-lines: "error"
  no-nested-ternary: "error"
  no-param-reassign: "error"
  no-plusplus: ["error", { allowForLoopAfterthoughts: true }]
  no-prototype-builtins: "error"
  no-return-assign: "error"
  no-shadow: "error"
  no-unused-vars: ["error", { "argsIgnorePattern": "^_", ignoreRestSiblings: true }]
  no-use-before-define: "error"
  no-useless-concat: "error"
  padded-blocks: "error"
  quotes: ["error", "double"]
  radix: "error"
  require-await: "error"
  semi: ["error", "always"]
  semi-spacing: "error"
  space-before-function-paren: ["error", "always"]
  spaced-comment: ["error", "always"]
  vars-on-top: "error"
  # Extra rules
  extra-rules/no-commented-out-code: "error"
  extra-rules/potential-point-free: "warn"
  standard/no-callback-literal: "error"
  # React-specific rules
  react/boolean-prop-naming: "error"
  react/default-props-match-prop-types: "error"
  react/destructuring-assignment: ["error", "always"]
  react/jsx-boolean-value: ["error", "always"]
  react/jsx-child-element-spacing: "error"
  react/jsx-closing-tag-location: "error"
  react/jsx-curly-spacing: ["error", {"when": "never", "children": true}]
  react/jsx-fragments: ["error", "syntax"]
  react/jsx-handler-names: "error"
  react/jsx-key: "error"
  react/jsx-no-bind: "error"
  react/jsx-no-comment-textnodes: "error"
  react/jsx-no-duplicate-props: "error"
  react/jsx-no-target-blank: "error"
  react/jsx-one-expression-per-line: "error"
  react/jsx-pascal-case: "error"
  react/jsx-props-no-multi-spaces: "error"
  react/jsx-sort-default-props: "error"
  react/jsx-sort-props: off
  react/jsx-space-before-closing: "error"
  react/no-access-state-in-setstate: "error"
  react/no-children-prop: "error"
  react/no-danger: "error"
  react/no-deprecated: "error"
  react/no-did-mount-set-state: "error"
  react/no-did-update-set-state: "error"
  react/no-direct-mutation-state: "error"
  react/no-find-dom-node: "error"
  react/no-is-mounted: "error"
  react/no-multi-comp: "error"
  react/no-redundant-should-component-update: "error"
  react/no-this-in-sfc: "error"
  react/no-typos: "warn"
  react/no-unknown-property: "error"
  react/no-unsafe: "error"
  react/no-unused-prop-types: "error"
  react/no-unused-state: "error"
  react/no-will-update-set-state: "error"
  react/prefer-es6-class: "error"
  react/prop-types: "error"
  react/react-in-jsx-scope: "error"
  react/require-default-props: "error"
  react/require-render-return: "error"
  react/self-closing-comp: "error"
  react/sort-comp: "error"
  react/sort-prop-types: "error"
  react/state-in-constructor: ["error", "never"]
  react/style-prop-object: "error"
  react/void-dom-elements-no-children: "error"
  # React Native-specific rules
  react-native/no-color-literals: "error"
  react-native/no-inline-styles: "error"
  react-native/no-raw-text: "error"
  react-native/no-unused-styles: "error"
  react-native/sort-styles: ["error", "asc", { ignoreStyleProperties: true }]
  react-native/split-platform-components: "error"
